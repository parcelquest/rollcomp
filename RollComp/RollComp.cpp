// RollComp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "RollComp.h"
#include "Prodlib.h"
#include "Utils.h"
#include "Getopt.h"
#include "Logs.h"
#include "CountyInfo.h"

CWinApp theApp;
using namespace std;

char     acRawTmpl[_MAX_PATH], acBakTmpl[_MAX_PATH], acIniFile[_MAX_PATH], acMapPath[_MAX_PATH], acMapExt[8],
         asCntyCodes[64][8], acToday[32], acLogPath[_MAX_PATH]; 
char     *apTokens[64];
int      iTokens, iSkip, lToday, lRecCnt, iRecLen, iFileCnt, iCounties;
bool     bSendMail, bOverwriteLogfile, bDebug, bDiff, bMissingMap, bMissingIndex, bRunAll;


/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: RollComp -C<County code> [-D] [-M] [-m] [-O]\n");
   printf("     -D: generate diff files (new & del APN)\n");
   printf("     -M: generate missing index file\n");
   printf("     -m: generate missing map file\n");
   printf("     -O: overwrite log file\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void parseCmd(int argc, char* argv[])
{
   char  chOpt;                // gotten option character
   char *pszParam;

   bOverwriteLogfile = false;
   bSendMail = false;
   bDiff = false;
   bMissingMap = false;
   bMissingIndex = false;
   bRunAll = false;
   //acInfile[0] = acOutfile[0] = 0;

   while (1)
   {
      chOpt = GetOption(argc, argv, "C:DMmEOS:?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'C':   // county code
               if (pszParam != NULL)
               {
                  if (!_memicmp(pszParam, "ALL", 3))
                     bRunAll = true;
                  else
                  {
                     iTokens = ParseString(pszParam, ',', 58, apTokens);
                     if (iTokens > 0)
                     {
                        iCounties = iTokens;
                        for (int iTmp = 0; iTmp < iTokens; iTmp++)
                           strcpy(asCntyCodes[iTmp], apTokens[iTmp]);
                     }
                  }
               } else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'D':   // Generate diff files
               bDiff = true;
               break;

            case 'M':   // Generate missing map list
               bMissingMap = true;
               break;

            case 'm':   // Generate missing index list
               bMissingIndex = true;
               break;

            case 'E':   // Email if error occurs
               bSendMail = true;
               break;

            case 'O':   // Overwrite logfile
               bOverwriteLogfile = true;
               break;

            case 'S':   // Skip records
               if (pszParam != NULL)
                  iSkip = atoi(pszParam);
               else
                  Usage();
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         printf("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }
}

/*********************************** initProg ********************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int initProg()
{
   char  acTmp[_MAX_PATH], acLogFile[_MAX_PATH];
   int   iRet;

   // Get today date - yyyymmdd
   getCurDate(acToday);

   // System log
   GetPrivateProfileString("System", "LogPath", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acLogFile, "%s\\RollComp_%s.log", acTmp, acToday);

   // Open log file
   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   // Set debug flag
   GetPrivateProfileString("System", "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   // Output path
   GetPrivateProfileString("Output", "LogPath", "", acLogPath, _MAX_PATH, acIniFile);

   // Get raw file name
   GetPrivateProfileString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   GetPrivateProfileString("Data", "BakFile", "", acBakTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);
   iSkip = GetPrivateProfileInt("Data", "SkipRecs", 1, acIniFile);
   GetPrivateProfileString("Data", "MapPath", "", acMapPath, _MAX_PATH, acIniFile);
   GetPrivateProfileString("Data", "MapExt", "PQM", acMapExt, _MAX_PATH, acIniFile);

   // Get county info
   GetPrivateProfileString("System", "CountyTbl", "CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = loadCountyInfoCsv(acTmp);

   return iRet;
}

/******************************************************************************/

int findMissingIndex(char *pCnty)
{
   char     *pTmp, acBuf[2048], acOutbuf[_MAX_PATH], sMapLink[32], sLastBook[8], sMaxBook[8];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn;
   FILE     *fdOut;

   int      iTmp, iBookLen;
   DWORD    nBytesRead;
   BOOL     bRet;
   long     lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R", 1);
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "S", 1);

   // Find current file for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         strcpy(acRawFile, acOutFile);
      else
      {
         LogMsg("***** Source file %s or %s not available.  Please recheck!", acRawFile, acOutFile);
         return 1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   sprintf(acOutFile, "%s\\%s_MissingIndex.log", acLogPath, pCnty);
   LogMsg("Open output file %s", acOutFile);
   if (!(fdOut = fopen(acOutFile, "w")))
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   for (iTmp = 0; iTmp < iSkip; iTmp++)
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

   // Start loop
   iTmp = getApnLen(pCnty, &iBookLen);
   if ((pTmp = getMaxBook(pCnty)))
      strcpy(sMaxBook, pTmp);
   else
      strcpy(sMaxBook, "9999");
   sLastBook[iBookLen] = 0;
   lRecCnt = 0;

   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
      {
         CloseHandle(fhIn);
         fhIn = 0;

         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge Other values: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
         } else
            break;
      }

      // Stop at last known physical book
      if (memcmp(acBuf, sMaxBook, iBookLen) > 0)
         break;

      iTmp = memcmp(acBuf, sLastBook, iBookLen);
      if (iTmp)
      {
         memcpy(sLastBook, acBuf, iBookLen);
         memcpy(sMapLink, &acBuf[OFF_IMAPLINK], SIZ_IMAPLINK);
         myTrim(sMapLink, SIZ_IMAPLINK);
         sprintf(acTmpFile, "%s\\%s\\bk%s.%s", acMapPath, pCnty, sMapLink, acMapExt);
         if (_access(acTmpFile, 0))
         {
            sprintf(acOutbuf, "%s\n", sLastBook);
            fputs(acOutbuf, fdOut);
            if (bDebug)
               LogMsg0(acTmpFile);
            lRecCnt++;
         } 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total records output:       %u\n", lRecCnt);

   return iTmp;
}

/******************************************************************************/

int findMissingMap(char *pCnty)
{
   char     *pTmp, acBuf[2048], acOutbuf[_MAX_PATH], acLastMapLink[32], sMaxBook[16];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];

   HANDLE   fhIn;
   FILE     *fdOut;

   int      iTmp, iBookLen;
   DWORD    nBytesRead;
   BOOL     bRet;
   long     lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R", 1);
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "S", 1);

   // Find current file for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         strcpy(acRawFile, acOutFile);
      else
      {
         LogMsg("***** Source file %s or %s not available.  Please recheck!", acRawFile, acOutFile);
         return 1;
      }
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening raw file: %s\n", acRawFile);
      return 3;
   }

   // Open Output file
   sprintf(acOutFile, "%s\\%s_MissingMap.log", acLogPath, pCnty);
   LogMsg("Open output file %s", acOutFile);
   if (!(fdOut = fopen(acOutFile, "w")))
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   for (iTmp = 0; iTmp < iSkip; iTmp++)
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

   // Start loop
   lRecCnt = 0;
   acLastMapLink[SIZ_MAPLINK] = 0;
   iTmp = getApnLen(pCnty, &iBookLen);
   if ((pTmp = getMaxBook(pCnty)))
      strcpy(sMaxBook, pTmp);
   else
      strcpy(sMaxBook, "9999");

   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
      {
         CloseHandle(fhIn);
         fhIn = 0;

         // Check for next input
         cFileCnt++;
         acRawFile[strlen(acRawFile)-1] = cFileCnt | 0x30;
         if (!_access(acRawFile, 0))
         {
            // Open next Input file
            LogMsg("Open input file %s", acRawFile);
            fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
            {
               LogMsg("***** Error opening input file to merge Other values: %s\n", acRawFile);
               break;
            }
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
         } else
            break;
      }

      // Stop at last known physical book
      if (memcmp(acBuf, sMaxBook, iBookLen) > 0)
         break;

      iTmp = memcmp(&acBuf[OFF_MAPLINK], acLastMapLink, SIZ_MAPLINK);
      if (iTmp && acBuf[OFF_MAPLINK] > ' ')
      {
         memcpy(acLastMapLink, &acBuf[OFF_MAPLINK], SIZ_MAPLINK);
         sprintf(acTmpFile, "%s\\%s\\bk%s.%s", acMapPath, pCnty, acLastMapLink, acMapExt);
         remChar(acTmpFile, ' ');
         if (_access(acTmpFile, 0))
         {
            pTmp = strchr(acLastMapLink, '\\');
            sprintf(acOutbuf, "%s\n", ++pTmp);
            fputs(acOutbuf, fdOut);
            if (bDebug)
               LogMsg0(acTmpFile);
            lRecCnt++;
         } 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total records output:       %u\n", lRecCnt);

   return iTmp;
}

/******************************************************************************/

int RollCmpOne(char *pCnty, char *pFile1, char *pFile2)
{
   return 0;
}

/******************************************************************************/

int RollCmpAll()
{
   return 0;
}
/********************************** RollCompare ******************************
 *
 * This function compare current R01 (or S01) file against last run.  It will
 * generate a file with all new APN and a file with all deleted APN.
 *
 * Return 0 if successful.
 *
 ****************************************************************************/

int RollCompare(char *pCnty, char *pMode, int iFileNum)
{
   char     acBuf1[2048], acBuf2[2048], 
            sFile1[_MAX_PATH], sFile2[_MAX_PATH], acNewApnFile[_MAX_PATH], acDelApnFile[_MAX_PATH];
   HANDLE   fhIn1, fhIn2;
   FILE     *fdNew, *fdDel;

   int      iApnLen, iCmp, iNewRec=0, iUpdRec=0, iDelRec=0, lCnt=0;
   DWORD    nBytesRead;
   BOOL     bRet, bEof1, bEof2;

   sprintf(acNewApnFile, "%s\\%s_NewApn.%s", acLogPath, pCnty, acToday);
   sprintf(acDelApnFile, "%s\\%s_DelApn.%s", acLogPath, pCnty, acToday);
   sprintf(sFile1, acRawTmpl, pCnty, pCnty, "S", iFileNum);
   sprintf(sFile2, acRawTmpl, pCnty, pCnty, "R", iFileNum);

   // Find current file for processing
   if (_access(sFile2, 0))
   {
      if (!_access(sFile1, 0))
         strcpy(sFile2, sFile1);
      else
      {
         LogMsg("***** Source file %s or %s not available.  Please recheck!", sFile2, sFile1);
         return 1;
      }
   }

   // Last run backup file
   sprintf(sFile1, acBakTmpl, pCnty, pCnty, "B", iFileNum);
   if (_access(sFile1, 0))
   {
      LogMsg("***** Source file %s not available.  Please recheck!", sFile1);
      return 1;
   }

   // Open old file
   LogMsg("Open last run file %s", sFile1);
   fhIn1 = CreateFile(sFile1, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn1 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening current file: %s\n", sFile1);
      return 4;
   }

   // Open new file
   LogMsg("Open current file %s", sFile2);
   fhIn2 = CreateFile(sFile2, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn2 == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening current file: %s\n", sFile2);
      return 3;
   }

   // Open Output file
   LogMsg("Create new APN file %s", acNewApnFile);
   if (!(fdNew = fopen(acNewApnFile, pMode)))
   {
      LogMsg("***** Error creating output file: %s\n", acNewApnFile);
      return 4;
   }

   LogMsg("Create deleted APN file %s", acDelApnFile);
   if (!(fdDel = fopen(acDelApnFile, pMode)))
   {
      LogMsg("***** Error creating deleted file: %s\n", acDelApnFile);
      return 4;
   }

   // Init variables
   iApnLen = getApnLen(pCnty);

   // Start loop
   bRet = ReadFile(fhIn2, acBuf2, iRecLen, &nBytesRead, NULL);

   // Compare loop
   bEof1 = bEof2 = false;
   while (!bEof1 && !bEof2)
   {
      bRet = ReadFile(fhIn1, acBuf1, iRecLen, &nBytesRead, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u", lCnt, iDelRec, iNewRec);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", sFile1, GetLastError());
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
      {
         bEof1 = true;
         break;
      }

ReCompare:
      iCmp = memcmp(acBuf1, acBuf2, iApnLen);
      if (!iCmp)
      {
         // If same APN, read new record
         bRet = ReadFile(fhIn2, acBuf2, iRecLen, &nBytesRead, NULL);
      } else if (iCmp > 0)
      {  // new record in File2
         if (acBuf2[iApnLen-1] == ' ')
         {
            acBuf2[iApnLen-1] = '\n';
            acBuf2[iApnLen] = '\0';
         } else
         {
            acBuf2[iApnLen] = '\n';
            acBuf2[iApnLen+1] = '\0';
         }
         fputs(acBuf2, fdNew);

         // Get next input record from file2
         bRet = ReadFile(fhIn2, acBuf2, iRecLen, &nBytesRead, NULL);
         iNewRec++;
      } else
      {  // retired record - skip 
         iDelRec++;
         if (acBuf1[iApnLen-1] == ' ')
         {
            acBuf1[iApnLen-1] = '\n';
            acBuf1[iApnLen] = '\0';
         } else
         {
            acBuf1[iApnLen] = '\n';
            acBuf1[iApnLen+1] = '\0';
         }
         fputs(acBuf1, fdDel);
      }

      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", sFile2, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
      {
         bEof2 = true;
         break;
      }
      
      // Old APN > new APN, let recompare
      if (iCmp > 0)
         goto ReCompare;
   }

   // If file1 is not EOF, mark remaining APN to be deleted
   while (!bEof1)
   {
      // Output APN rec
      iDelRec++;
      if (acBuf1[iApnLen-1] == ' ')
      {
         acBuf1[iApnLen-1] = '\n';
         acBuf1[iApnLen] = '\0';
      } else
      {
         acBuf1[iApnLen] = '\n';
         acBuf1[iApnLen+1] = '\0';
      }
      fputs(acBuf1, fdDel);

      // Read next rec
      bRet = ReadFile(fhIn1, acBuf1, iRecLen, &nBytesRead, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u", lCnt, iDelRec, iNewRec);

      if (iRecLen != nBytesRead)
         bEof1 = true;
   }

   while (!bEof2)
   {
      if (acBuf2[iApnLen-1] == ' ')
      {
         acBuf2[iApnLen-1] = '\n';
         acBuf2[iApnLen] = '\0';
      } else
      {
         acBuf2[iApnLen] = '\n';
         acBuf2[iApnLen+1] = '\0';
      }
      fputs(acBuf2, fdNew);

      bRet = ReadFile(fhIn2, acBuf2, iRecLen, &nBytesRead, NULL);
      iNewRec++;

      if (!(++lCnt % 1000))
         printf("\r%u\t%u\t%u", lCnt, iDelRec, iNewRec);

      if (iRecLen != nBytesRead)
         bEof2 = true;
   }

   // Close files
   if (fhIn1)
      CloseHandle(fhIn1);
   if (fhIn2)
      CloseHandle(fhIn2);

   if (fdDel)
      fclose(fdDel);
   if (fdNew)
      fclose(fdNew);

   printf("\r%u\t%u\t%u\n", lCnt, iDelRec, iNewRec);
   LogMsgD("Total records read:         %u", lCnt);
   LogMsgD("Total new records:          %u", iNewRec);
   LogMsgD("Total deleted records:      %u", iDelRec);

   return 0;
}

/******************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int   iRet, iTmp;
   char  acTmp[_MAX_PATH];

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			return 1;
		}
	}

   iRet = LoadString(theApp.m_hInstance, IDS_APP_TITLE, acTmp, 64);
	printf("%s\n", acTmp);

   if (argc < 2)
      Usage();

   _getcwd((char *)&acIniFile[0], _MAX_PATH);
   strcat(acIniFile, "\\RollComp.ini");

   // Parse command line
   parseCmd(argc, argv);

   // Init globals
   iRet = initProg();

   LogMsg(acTmp);

   if (bRunAll)
   {
      iRet = setCountyReady(NULL);
   } else
   {
      for (iTmp = 0; iTmp < iCounties; iTmp++)
         iRet = setCountyReady(asCntyCodes[iTmp]);
   }

   char *pCnty;
   int   iCnty=0, iNext;
   while (pCnty = getNextCnty(iCnty, &iNext))
   {
      LogMsgD("\nProcessing %s", pCnty);

      if (bMissingMap)
      {
         LogMsg("Finding missing map ...");
         iRet = findMissingMap(pCnty);
      }

      if (bMissingIndex)
      {
         LogMsg("Finding missing index ...");
         iRet = findMissingIndex(pCnty);
      }

      if (bDiff)
      {
         LogMsg("Roll Compare %s ...", pCnty);
         iRet = RollCompare(pCnty, "w", 1);
         if (!memcmp(pCnty, "LAX", 3))
         {
            iRet = RollCompare(pCnty, "a+", 2);
            iRet = RollCompare(pCnty, "a+", 3);
         }
      }

      iCnty = iNext;
   }

   close_log();
	return iRet;
}
