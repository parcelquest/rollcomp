#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "Logs.h"
#include "CountyInfo.h"

COUNTY_INFO m_asCountyInfo[64];

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int loadCountyInfoCsv(char *pCntyTbl)
{
   char     acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int      iRet=0, iTmp, iIdx;
   FILE     *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iIdx = 0;
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
            if (iTmp > 0)
            {
               strcpy(m_asCountyInfo[iIdx].acCntyCode, pFlds[FLD_CNTY_CODE]);
               strcpy(m_asCountyInfo[iIdx].acStdApnFmt, pFlds[FLD_APN_FMT]);
               strcpy(m_asCountyInfo[iIdx].acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
               strcpy(m_asCountyInfo[iIdx].acCntyID, pFlds[FLD_CNTY_ID]);
               strcpy(m_asCountyInfo[iIdx].acCntyName, pFlds[FLD_CNTY_NAME]);
               if (iTmp > FLD_YR_ASSD)
               {
                  strcpy(m_asCountyInfo[iIdx].acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(m_asCountyInfo[iIdx].acYearAssd, pFlds[FLD_YR_ASSD]);
               }
               iRet = atoi(m_asCountyInfo[iIdx].acCntyID);
               m_asCountyInfo[iIdx].iCntyID = iRet;
               sprintf(m_asCountyInfo[iIdx].acFipsCode, "06%.3d", iRet*2-1);
               m_asCountyInfo[iIdx].iApnLen = atoi(pFlds[FLD_APN_LEN]);
               m_asCountyInfo[iIdx].iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
               m_asCountyInfo[iIdx].iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
               m_asCountyInfo[iIdx].iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
               m_asCountyInfo[iIdx].isReady = 'N';
               iIdx++;
            } else
            {
               LogMsg("***** Bad county table file: %s", pCntyTbl);
            }
         } else
            break;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening county table %s", pCntyTbl);

   return iRet;
}

/******************************************************************************/

bool setCountyReady(char *pCntyCode)
{
   int   iIdx=0;
   bool  bRet=false;

   while (m_asCountyInfo[iIdx].isReady > ' ')
   {
      if (pCntyCode && !strcmp(m_asCountyInfo[iIdx].acCntyCode, pCntyCode))
      {
         m_asCountyInfo[iIdx].isReady = 'Y';
         bRet = true;
         break;
      } else if (!pCntyCode)
         m_asCountyInfo[iIdx].isReady = 'Y';
      iIdx++;
   }

   return bRet;
}

/******************************************************************************/

bool isCountyReady(char *pCntyCode)
{
   int   iIdx=0;
   bool  bRet=false;

   while (m_asCountyInfo[iIdx].isReady > ' ')
   {
      if (!strcmp(m_asCountyInfo[iIdx].acCntyCode, pCntyCode))
      {
         if (m_asCountyInfo[iIdx].isReady == 'Y')
            bRet = true;
         break;
      }
      iIdx++;
   }

   return bRet;
}

/******************************************************************************/

int getApnLen(char *pCntyCode, int *piBookLen, int *piPageLen)
{
   int   iIdx=0, iRet=0;
   bool  bRet=false;

   while (m_asCountyInfo[iIdx].isReady > ' ')
   {
      if (!strcmp(m_asCountyInfo[iIdx].acCntyCode, pCntyCode))
      {
         if (m_asCountyInfo[iIdx].isReady == 'Y')
         {
            iRet = m_asCountyInfo[iIdx].iApnLen;
            if (piBookLen)
               *piBookLen = m_asCountyInfo[iIdx].iBookLen;
            if (piPageLen)
               *piPageLen = m_asCountyInfo[iIdx].iPageLen;
         }
         break;
      }
      iIdx++;
   }

   return iRet;
}

/******************************************************************************/

char *getMaxBook(char *pCntyCode)
{
   int   iIdx=0;
   char  *pRet=NULL;

   while (m_asCountyInfo[iIdx].isReady > ' ')
   {
      if (!strcmp(m_asCountyInfo[iIdx].acCntyCode, pCntyCode))
      {
         pRet = m_asCountyInfo[iIdx].acSpcApnFmt[0];
         break;
      }
      iIdx++;
   }

   return pRet;
}

/******************************************************************************/

char *getNextCnty(int iStart, int *iNext)
{
   int   iIdx;
   char  *pRet=NULL;

   iIdx = iStart;
   while (m_asCountyInfo[iIdx].isReady > ' ')
   {
      if (m_asCountyInfo[iIdx].isReady == 'Y')
      {
         pRet = m_asCountyInfo[iIdx].acCntyCode;
         *iNext = iIdx+1;
         break;
      }
      iIdx++;
   }

   return pRet;
}
