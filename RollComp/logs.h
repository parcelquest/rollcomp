#ifndef _LOGS_H
#define _LOGS_H

#define DEFAULT_LOG "C:\\Temp\\ProdLib.log"

void open_log(char *pLogfile, char *pMode, char *pFrom=NULL);
void close_log();
void LOG_ERROR(char *fname, int lineno, char *fmt, ...);
void LOG(char *fname, int lineno, char *fmt, ...);
void LogMsg(char *fmt, ...);
void LogMsg0(char *fmt, ...);
void LogMsg1(char *fmt, ...);
void LogMsgD(char *fmt, ...);
char *DateStamp();
char *getLastErrorLog(void);

#endif
