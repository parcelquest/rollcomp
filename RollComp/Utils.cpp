#include "stdafx.h"
#include "prodlib.h"
#include "Utils.h"
#include "Logs.h"

char asMonth[12][4]=
{
   "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
};

int   monthdays[] = {31,28,31,30,31,30,31,31,30,31,30,31};
char  m_sMachine[64], m_sDataSrc[128];

/********************************* myTrim() *********************************
 *
 ****************************************************************************/

char *myTrim(char *pString)
{
   int i;

   // remove trailing spaces and tabs and other non-printables
   i = strlen(pString)-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return pString;
}

char *myLTrim(char *pString, int iLen)
{
   char  *pTmp;

   pTmp = pString;
   if (iLen > 0)
      *(pTmp + iLen) = 0;

   while (*pTmp && *pTmp == ' ')
      pTmp++;

   strcpy(pString, pTmp);
   return pString;
}

char *myTrim(char *pString, int iLen)
{
   int   i;

   *(pString + iLen) = 0;
   if (iLen < 2)
      return pString;

   // remove trailing spaces and tabs and other non-printables
   i = iLen-1;
   while (i >= 0)
   {
      if ((pString[i] > ' ') && (pString[i] <= '~'))
         break;
      pString[i] = 0;
      i--;
   }

   return pString;
}

/******************************** myBTrim() ********************************
 *
 * Remove both leading and trailing spaces
 *
 ***************************************************************************/

char *myBTrim(char *pString, int iLen)
{
   char *pTmp;
   int   i;

   if (iLen > 0)
      *(pString+iLen) = 0;

   // Remove leading space
   pTmp = pString;
   while (*pTmp && *pTmp <= ' ')
      pTmp++;

   // Remove trailing space
   i = strlen(pTmp)-1;
   while (i >= 0)
   {
      if ((pTmp[i] > ' ') && (pTmp[i] <= '~'))
         break;
      pTmp[i] = 0;
      i--;
   }

   strcpy(pString, pTmp);
   return pString;
}

/******************************** isPrintable() *****************************
 *
 ****************************************************************************/

int isPrintable(char *pBuf, int iLen)
{
   int iRet;

   for (iRet = 0; iRet < iLen; iRet++)
   {
      if (*pBuf < ' ' || *pBuf > 'z')
         break;
      pBuf++;
   }
   return iRet;
}

/******************************** isBlank() ********************************/

bool isBlank(LPCSTR pStr, int iLen)
{
   int  iTmp, iIdx;
   bool bRet = true;

   if (iLen == 0)
      iTmp = strlen(pStr);
   else
      iTmp = iLen;

   for (iIdx = 0; iIdx < iTmp; iIdx++)
   {
      if (*pStr != ' ')
      {
         bRet = false;
         break;
      }
      pStr++;
   }

   return bRet;
}

/*****************************************************************************/

char isChar(LPCSTR pBuf, int iLen)
{
   int   iIdx;
   char  cChar = ' ';

   for (iIdx = 0; iIdx < iLen; iIdx++)
   {
      if (*pBuf >= 'A' && *pBuf <= 'Z')
      {
         cChar = *pBuf;
         break;
      }
      pBuf++;
   }
   return cChar;
}

/*****************************************************************************/

bool isNumber(LPSTR pNumber)
{
   bool bRet=false;

   while (*pNumber)
   {
      if (isdigit(*pNumber) || *pNumber == '-')
         bRet = true;
      else
      {
         bRet = false;
         break;
      }
      pNumber++;
   }

   return bRet;
}

/******************************** blankPad() *******************************/

void blankPad(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
}

// Terminate output string by zero
void blankPadz(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
   *pTmp = 0;
}

// Remove CRLF before padding
void blankPad_RemCRLF(char *pBuf, int iLen)
{
   char *pTmp;
   int   iTmp=0;

   pTmp = pBuf;
   while (*pTmp && iTmp < iLen)
   {
      if (*pTmp == 10 || *pTmp == 13)
         *pTmp = ' ';
      pTmp++;
      iTmp++;
   }
   while (iTmp++ < iLen)
      *pTmp++ = ' ';
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Return string length
 *
 ***************************************************************************/

int blankRem(char *pBuf, int iLen)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);  
   return iTmp;
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Special version to be used with CString input
 *
 ***************************************************************************/

int blankRem(CString& pStr)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   strcpy(acTmp, pStr);

   // Remove leading space
   pTmp = &acTmp[0];
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   pStr = acTmp;  
   return iTmp;
}

/******************************** blankRem() *******************************
 *
 * Reduce multi-space into single one
 * Also reduce blanks that preceeded or followed by checked char.
 *
 ***************************************************************************/

int blankRem(char *pBuf, char bChkChar, int iLen)
{
   char *pTmp, acTmp[8192];
   int   iTmp=0;
   bool  bSpace = false;

   if (iLen > 0)
      *(pBuf+iLen) = 0;

   // Remove leading space
   pTmp = pBuf;
   while (*pTmp == ' ')
      pTmp++;

   // Remove extra space in the middle
   while (*pTmp)
   {
      if (*pTmp == ' ')
      {
         if (!bSpace)
         {
            bSpace = true;
            acTmp[iTmp++] = ' ';
         }
      } else if ((*pTmp == bChkChar) && bSpace)
      {
         acTmp[iTmp-1] = *pTmp;
      } else
      {
         acTmp[iTmp++] = *pTmp;
         bSpace = false;
      }

      pTmp++;
   }

   if (iTmp > 0 && acTmp[iTmp-1] <= ' ')
      iTmp--;
   acTmp[iTmp] = 0;
   strcpy(pBuf, acTmp);  
   return iTmp;
}

/******************************** quoteRem() *******************************
 *
 * Remove all single and double quotes in string
 *
 ***************************************************************************/

void quoteRem(char *pBuf, int iLen)
{
   char *pTmp;

   pTmp = pBuf;
   if (!iLen)
   {
      while (*pTmp)
      {
         if (*pTmp != 34 && *pTmp != 39)
            *(pBuf++) = *pTmp;
         pTmp++;
      }

      *pBuf = 0;
   } else
   {
      int iTmp, iRemove=0;
      for (iTmp = 0; iTmp < iLen; iTmp++)
      {
         if (*pTmp != 34 && *pTmp != 39)
            *(pBuf++) = *pTmp;
         else 
            iRemove++;
         pTmp++;         
      }
      while (iRemove-- > 0)
         *(pBuf++) = ' ';
   }
}

/********************************** atoin() *********************************/

int atoin(char *pStr, int iLen)
{
   char  acVal[32];
   int   iRet;

   if (!pStr || !*pStr)
      return 0;

   memcpy(acVal, pStr, iLen);
   acVal[iLen] = '\0';
   iRet = atoi(acVal);
   return iRet;
}

/********************************** astol() *********************************/

long astol(char *pStr)
{
   char  acVal[32];
   long  lRet=0;

   if (!pStr || !*pStr)
      return 0;

   while (*pStr)
   {
      if (isdigit(*pStr))
         acVal[lRet++] = *pStr;
      pStr++;
   }
   acVal[lRet] = '\0';
   lRet = atol(acVal);
   return lRet;
}

/********************************** atofn() *********************************/

double atofn(char *pStr, int iLen)
{
   char     acVal[32], *pTmp;
   double   dRet;
   int      iTmp = 0, iTmp1 = 0;

   if (!pStr || !*pStr)
      return 0;

   pTmp = pStr;
   while (iTmp < iLen && *pTmp)
   {
      if (isdigit(*pTmp) || *pTmp == '.')
         acVal[iTmp1++] = *pTmp;
      iTmp++;
      pTmp++;
   }

   acVal[iTmp1] = '\0';
   try
   {
      dRet = atof(acVal);
   } catch(...)
   {
      dRet = 0;
   }
   return dRet;
}

/********************************** isValidYMD *******************************
 *
 * Check for YYYYMMDD format
 *
 *****************************************************************************/

bool isValidYMD(char *pDate, bool bChkDate)
{
   bool bRet=true;
   int  iTmp, iMonth, iYear, iDay, iCurYear;

   iCurYear = getCurYear();

   // Validate year
   iYear = atoin(pDate, 4);
   if (iYear < 1900 || iYear > iCurYear)
      bRet = false;
   else
   {
      // Validate month
      iMonth = atoin(pDate+4, 2);
      if (iMonth < 1 || iMonth > 12)
         bRet = false;
      else if (bChkDate)
      {
         // Validate date
         iTmp = gdate_monthdays(iMonth, iYear);
         iDay = atoin(pDate+6, 2);
         if (iDay < 1 || iDay > iTmp)
            bRet = false;
      }
   }

   return bRet;
}

/********************************** strUpper() *********************************
 *
 * If string length is provided, output string will be padded with spaces and
 * not terminated with 0.
 *
 *******************************************************************************/

char *strUpper(char *pInStr, char *pOutStr, int iLen)
{
   char  *pIn, *pOut;
   int   iCnt;

   pIn = pInStr;
   pOut = pOutStr;

   if (iLen == 0)
   {
      while (*pIn)
      {
         if (isalpha(*pIn))
            *pOut++ = *pIn++ & 0x5F;
         else
            *pOut++ = *pIn++;
      }
      *pOut = 0;
   } else
   {
      for (iCnt = 0; iCnt < iLen; iCnt++)
      {
         if (isalpha(*pIn))
            *pOut++ = *pIn++ & 0x5F;
         else if (*pIn)
            *pOut++ = *pIn++;
         else
            *pOut++ = ' ';
      }
   }

   return pOutStr;
}

/********************************* appendTxtFile *****************************
 *
 * Append file1 to file2
 * If file1 doesn't exist, return -1.  Otherwise returns 0
 *
 *****************************************************************************/

int appendTxtFile(char *pFile1, char *pFile2)
{
   int   iRet;
   FILE  *fdIn, *fdOut;
   char  *pTmp, acTmp[4096];

   if (_access(pFile1, 0))
      return -1;

   fdIn = fopen(pFile1, "r");
   fdOut = fopen(pFile2, "a+");

   while (!feof(fdIn))
   {
      pTmp = fgets(acTmp, 4096, fdIn);
      if (pTmp)
         iRet = fputs(acTmp, fdOut);
      else
         break;
   }

   fclose(fdIn);
   fclose(fdOut);
   return 0;
}

/********************************* appendFixFile *****************************
 *
 * Append file1 to file2
 *
 *****************************************************************************/

int appendFixFile(char *pFile1, char *pFile2, int iRecLen)
{
   int   iRet=0;

   return iRet;
}

/********************************** replChar() ******************************
 *
 * Replace a char in a string and drop CRLF.  Return number of replacements.
 *
 ****************************************************************************/

int replChar(char *pBuf, unsigned char cSrch, char cRepl)
{
   int iRet=0;

   if (cSrch == 10 || cSrch == 13)
   {
      while (*pBuf)
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet++;
         }
         pBuf++;
      }
   } else
   {
      while (*pBuf && (*pBuf != 10 && *pBuf != 13))
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet++;
         }
         pBuf++;
      }
   }
   if (*pBuf == 10 || *pBuf == 13)
      *pBuf = 0;

   return iRet;
}

/********************************** replChar() ******************************
 *
 * This version requires string length or string must ended with LF or NULL
 *
 ****************************************************************************/

int replChar(char *pBuf, unsigned char cSrch, char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf && *pBuf != 10)  // scan till CR
      {
         if ((unsigned char)*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

int replUChar(unsigned char *pBuf, unsigned char cSrch, unsigned char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if (*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf && *pBuf != 10)  // scan till CR
      {
         if (*pBuf == cSrch)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

/********************************** replNull() ******************************
 *
 * This version requires string length or string must ended with LF or NULL
 *
 ****************************************************************************/

int replNull(char *pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;

   if (iLen > 0)
   {
      for (iTmp = 0; iTmp < iLen; iTmp++, pBuf++)
      {
         if (!*pBuf)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
      }
   } else
   {
      iTmp = 0;
      while (*pBuf != 10)  // scan till CR
      {
         if (!*pBuf)
         {
            *pBuf = cRepl;
            iRet = iTmp;
         }
         pBuf++;
         iTmp++;
      }
   }

   return iRet;
}

/******************************* replQuoteEx() *****************************
 *
 * Replace all char quotes and unprintable char with blank
 *
 ***************************************************************************/

void replQuoteEx(char *pBuf, int iLen)
{
   int iTmp, iRet=0;

   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || *pBuf == 39)
      {
         *pBuf = ' ';
         iRet = iTmp;
      }
   }
}

/******************************* replCharEx() ******************************
 *
 * Replace all char less than or equal to searched char
 *
 ***************************************************************************/

int replCharEx(char *pBuf, char cSrch, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf <= cSrch)
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

/******************************* replNonNum() ******************************
 *
 * Replace all non-digit with specified char cRepl (default blank space).
 *
 ***************************************************************************/

int replNonNum(char *pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (!isdigit(*pBuf))
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

/******************************* replCharEx() ******************************
 *
 * Scan strings for characters in specified character sets and replace them
 * with cRepl (default blank space). This version will remove extra space 
 * if bRemBlank=true.  
 *
 * Return 0 if nothing change, 1 if found in charset.
 *
 ***************************************************************************/

int replCharEx(char *pBuf, char *pCharSet, char cRepl, int iLen, bool bRemBlank, bool bReplUnPrt)
{
   char *pTmp, acTmp[2048];
   bool  bSpace = false;
   int   iRet=0;

   if (!pBuf || !*pBuf)
      return iRet;

   if (iLen > 0)
      *(pBuf+iLen) = 0;
   else if (strlen(pBuf) > 2047)
      return iRet;

   strcpy(acTmp, pBuf);
   pTmp = acTmp;
   if (bReplUnPrt)
   {
      while (*pTmp)
      {
         if (*pTmp < ' ' || (unsigned char)*pTmp > 126)  // ~
         {
            iRet = 2;
            *pTmp = cRepl;
         }
         pTmp++;
      }
   }

   pTmp = acTmp;
   while (pTmp)
   {
      pTmp = strpbrk(pTmp, pCharSet);
      if (pTmp)
      {
         iRet = 1;
         *pTmp = cRepl;
      }
   }

   if (bRemBlank && cRepl == ' ')
      blankRem(acTmp);
   strcpy(pBuf, acTmp);

   return iRet;
}

/****************************** replSqlChar() ******************************
 *
 * Replace all char which may caused problem to SQL search.
 *
 ***************************************************************************/

int replSqlChar(LPSTR pBuf, char cRepl, int iLen)
{
   int   iBuflen, iTmp, iRet=0;
   LPSTR pTmp = pBuf;  

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pTmp++)
   {
      if (*pTmp < ' ' || *pTmp == '\\' || (unsigned char)*pTmp > 126) //'~'
      {
         *pBuf++ = cRepl;
         iRet = iTmp;
      } else if (*pTmp != 39)    // Drop single quote
         *pBuf++ = *pTmp;
      else
         iRet = iTmp;
   }
   *pBuf = 0;

   return iRet;
}

/**************************** replUnPrtChar() ******************************
 *
 * Replace all char less than or equal to searched char
 *
 ***************************************************************************/

int replUnPrtChar(LPSTR pBuf, char cRepl, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || (unsigned char)*pBuf > 126) //'~'
      {
         *pBuf = cRepl;
         iRet = iTmp;
      }
   }
   return iRet;
}

int findUnPrtChar(LPSTR pBuf, int iLen)
{
   int iTmp, iRet=0;
   int iBuflen;

   if (!iLen)
      iBuflen = strlen(pBuf);
   else
      iBuflen = iLen;

   for (iTmp = 0; iTmp < iBuflen; iTmp++, pBuf++)
   {
      if (*pBuf < ' ' || (unsigned char)*pBuf > 126) //'~'
         iRet = iTmp;
   }
   return iRet;
}

/********************************** replStr() *******************************
 *
 * Replace a substring
 *
 ****************************************************************************/

int replStr(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl)
{
   char  acTmp[4096], *pTmp;
   int   iRet=0;

   pTmp = strstr(pStr, pFind);
   if (pTmp)
   {
      *pTmp = 0;
      iRet = sprintf(acTmp, "%s%s%s", pStr, pRepl, pTmp+strlen(pFind));
      strcpy(pStr, acTmp);
   }

   return iRet;
}


/******************************* replStrAll() *******************************
 *
 * Replace all occurences of a substring
 *
 ****************************************************************************/

int replStrAll(LPSTR pStr, LPCSTR pFind, LPCSTR pRepl)
{
   char  acTmp[4096], *pTmp;
   int   iRet=0;

   pTmp = strstr(pStr, pFind);
   while (pTmp)
   {
      *pTmp = 0;
      iRet = sprintf(acTmp, "%s%s%s", pStr, pRepl, pTmp+strlen(pFind));
      strcpy(pStr, acTmp);
      pTmp = strstr(pStr, pFind);
   }

   return iRet;
}

/***************************** remUnPrtChar() ******************************
 *
 * Remove all unprintable chars
 *
 ***************************************************************************/

int remUnPrtChar(LPSTR pBuf)
{
   char  *pTmp = pBuf;
   int   iCnt = 0;

   while (*pBuf)
   {
      if (*pBuf > 31 && (unsigned char)*pBuf < 126)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }
   *pTmp = 0;

   return iCnt;
}

/********************************** remChar() *******************************
 *
 * Remove all occurences of a char in a string.  Return number of chars output.
 *
 ****************************************************************************/

int remChar(char *pBuf, char cSrch)
{
   char  *pTmp = pBuf;
   int   iCnt = 0;

   while (*pBuf)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }
   *pTmp = 0;

   return iCnt;
}

int remChar(char *pBuf, char cSrch, int iBufLen)
{
   char  *pTmp = pBuf;
   int   iCnt = 0, iTmp;

   for (iTmp = 0; iTmp < iBufLen; iTmp++)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }

   iTmp = iCnt;
   while (iTmp++ < iBufLen)
      *pTmp++ = ' ';

   return iCnt;
}

int remUChar(unsigned char *pBuf, unsigned char cSrch, int iBufLen)
{
   unsigned char  *pTmp = pBuf;
   int   iCnt = 0, iTmp;

   for (iTmp = 0; iTmp < iBufLen; iTmp++)
   {
      if (*pBuf != cSrch)
      {
         *pTmp++ = *pBuf;
         iCnt++;
      }
      pBuf++;
   }

   iTmp = iCnt;
   while (iTmp++ < iBufLen)
      *pTmp++ = ' ';

   return iCnt;
}

/******************************** remCharEx() ******************************
 *
 * Scan strings for characters in specified character sets and remove them.
 *
 ***************************************************************************/

void remCharEx(char *pBuf, char *pCharSet)
{
   char *pTmp, *pStart, acTmp[4096], acOut[4096];
   bool  bSpace = false;

   if (!pBuf || !*pBuf)
      return;

   if (strlen(pBuf) > 4095)
      return;

   strcpy(acTmp, pBuf);
   pTmp = acTmp;
   acOut[0] = 0;
   while (pTmp)
   {
      pStart = pTmp;
      pTmp = strpbrk(pTmp, pCharSet);
      if (pTmp)
         *pTmp++ = 0;
      strcat(acOut, pStart);
   }

   strcpy(pBuf, acOut);
}

/********************************* replByte_EQ *******************************
 *
 * Replace all occurences of a character with another one in a file.  If bExact
 * is set, only matched byte is replaced.  If not, all bytes equal or smaller
 * will be replaced with blank.
 *
 *****************************************************************************/

long replByte_EQ(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   iLen = 8192;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == bSrchChar)
            acBuf[iTmp] = bReplChar;

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/*****************************************************************************/

long replByte_LE(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   iLen = 8192;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   // Open Output file
   fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return -2;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] <= bSrchChar)
            acBuf[iTmp] = bReplChar;

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/********************************* replaceByte *******************************
 *
 * Replace all occurences of a character with another one in a file.  It also
 * replaces double quotes with blanks.
 *
 *****************************************************************************/

long replaceByte(LPCSTR pInfile, char bSrchChar, char bReplChar, int iRecSize)
{
   int      iTmp;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192], acTmpFile[128], *pTmp;

   // Prepare temp file
   strcpy(acTmpFile, pInfile);
   pTmp = strrchr(acTmpFile, '.');
   if (pTmp)
      strcpy(pTmp, ".rpl");
   else
      strcat(acTmpFile, ".rpl");

   // Remove temp file is exist
   if (!_access(acTmpFile, 0))
      remove(acTmpFile);

   rename(pInfile, acTmpFile);

   fhIn = CreateFile(acTmpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 3;

   // Open Output file
   fhOut = CreateFile(pInfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
      return 4;

   if (iRecSize > 0)
      iLen = iRecSize;
   else
      iLen = 4096;

   while (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      if (!nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == bSrchChar)
            acBuf[iTmp] = bReplChar;
         else if (acBuf[iTmp] == '\"')
            acBuf[iTmp] = ' ';

         iTmp++;
      }

      bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return lCnt;
}

/********************************* getDate() ********************************
 *
 * Return current date + number of extended day.
 *
 ****************************************************************************/

/*
void getDate(char *pDate, long lExtend)
{
   CTime today = CTime::GetCurrentTime();
   CTimeSpan tsExtend(lExtend);

   today += tsExtend;
   strcpy(pDate, today.Format("%Y%m%d"));
}
*/

void getCurDate(char *pDate)
{
   SYSTEMTIME sSysTime;

   GetLocalTime(&sSysTime);
   if (pDate)
      sprintf(pDate, "%.4d%.2d%.2d", sSysTime.wYear, sSysTime.wMonth, sSysTime.wDay);
}

/*****************************************************************************/

int getCurYear(char *pYear)
{
   SYSTEMTIME sSysTime;

   GetLocalTime(&sSysTime);
   if (pYear)
      sprintf(pYear, "%.4d", sSysTime.wYear);
   return  sSysTime.wYear;
}

/*******************************************************************
 *
 * 1 = DD-MMM-YYYY   21-JUL-2005
 * 2 = MM/DD/YYYY    7/21/2005 0:00:00 or the like
 * 3 = mmm dd yyyy   Nov 10 2005 0:00:00
 * 4 = MM/DD/YY      07/21/05
 * 5 = MMDDYY        072105
 * 6 = YY2YYYY       99 or 02
 * 7 = YY/MM/DD      97/12/31
 * 8 = YYMMDD        971231
 * 9 = YYYY-MM-DD    1993-08-31 00:00:00
 * 10= MMDDYYYY      12102011
 * default = MM/DD/YYYY
 *
 * if Expected Year = 0, use current year for verification
 *
 *******************************************************************/

char *dateConversion(char *pFromDate, char *pToDate, int iFromFmt, int iExpYear)
{
   char  *pRet, *pTmp, acTmp[32];
   int   i, iMon, iDay, iYear, iDate, iCurYear;

   pRet = NULL;
   strcpy(pToDate, "        ");
   if (!pFromDate || !*pFromDate)
      return pRet;

   if (iExpYear > 0)
      iCurYear = iExpYear;
   else
      iCurYear = getCurYear(acTmp);

   switch (iFromFmt)
   {
      case DD_MMM_YYYY:       // 1
         for (i = 0; i < 12; i++)
            if (!memcmp(pFromDate+3, asMonth[i], 3))
               break;
         if (i < 12)
         {
            sprintf(pToDate, "%.4s%0.2d%.2s", pFromDate+7, i+1, pFromDate);
            pRet = pToDate;
         }
         break;

      case MM_DD_YYYY_1:      // 2 = m/d/yyyy
         strncpy(acTmp, pFromDate, 11);
         acTmp[11] = 0;
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         if (iMon == 0) iMon = 1;
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            if (iDate == 0) iDate = 1;
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               iCurYear = atoi(pRet);
               sprintf(pToDate, "%.4d%0.2d%0.2d", iCurYear, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;

      case MMM_DD_YYYY:       // 3 = Feb 01, 2016 or Feb 01 2016
         for (i = 0; i < 12; i++)
            if (!_memicmp(pFromDate, asMonth[i], 3))
               break;
         if (i < 12)
         {
            if (*(pFromDate+7) == ' ')
               sprintf(pToDate, "%.4s%0.2d%0.2d", pFromDate+8, i+1, atoin(pFromDate+4, 2));
            else
               sprintf(pToDate, "%.4s%0.2d%0.2d", pFromDate+7, i+1, atoin(pFromDate+4, 2));
            pRet = pToDate;
         }
         break;

      case MMDDYY1:           // 4 = mm/dd/yy
         strncpy(acTmp, pFromDate, 9);
         acTmp[9] = 0;
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         // TCB If 00 set it to 01
         if (iMon == 0) iMon = 1;
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            // TCB If 00 set it to 01
            if (iDate == 0) iDate = 1;
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               // Check for year with extra digit
               if (strlen(pRet) > 2)
                  *(pRet+2) = 0;
               i = atol(pRet);
               if (i+2000 > iCurYear)
                  sprintf(pToDate, "19%.2d%0.2d%0.2d", i, iMon, iDate);
               else
                  sprintf(pToDate, "20%.2d%0.2d%0.2d", i, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;

      case MMDDYY2:           // 5 = mmddyy
         i = atoin(pFromDate+4, 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d%.4s", i, pFromDate);
         else
            sprintf(pToDate, "20%.2d%.4s", i, pFromDate);
         pRet = pToDate;
         break;

      case YY2YYYY:           // 6
         i = atoin(pFromDate, 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d", i);
         else
            sprintf(pToDate, "20%.2d", i);
         pRet = pToDate;
         break;

      case YYMMDD1:           // 7
         if (*(pFromDate+2) == '/' && *(pFromDate+5) == '/')
         {
            i = atoin(pFromDate, 2);
            if (i+2000 > iCurYear)
               sprintf(pToDate, "19%.2s%.2s%.2s", pFromDate, pFromDate+3, pFromDate+6);
            else
               sprintf(pToDate, "20%.2s%.2s%.2s", pFromDate, pFromDate+3, pFromDate+6);
            pRet = pToDate;
         }
         break;

      case YYMMDD2:           // 8
         i = atoin(pFromDate, 2);
         if (i+2000 > iCurYear)
            sprintf(pToDate, "19%.2d%.4s", i, pFromDate+2);
         else
            sprintf(pToDate, "20%.2d%.4s", i, pFromDate+2);
         pRet = pToDate;
         break;

      case YYYY_MM_DD:        // 9
         if (*(pFromDate+4) == '-' && *(pFromDate+7) == '-')
         {
            sprintf(pToDate, "%.4s%.2s%.2s", pFromDate, pFromDate+5, pFromDate+8);
            pRet = pToDate;
         }
         break;

      case MMDDYYYY:
         iMon = atoin(pFromDate, 2);
         iDay = atoin(pFromDate+2, 2);
         iYear =atoin(pFromDate+4, 4);
         if (iYear > 0 && iYear <= iCurYear)
         {
            sprintf(pToDate, "%d%.2d%.2d", iYear, iMon, iDay);
            pRet = pToDate;
         }
         break;

      default:
         strcpy(acTmp, pFromDate);
         pTmp = strchr(acTmp, ' ');
         if (pTmp) *pTmp = 0;
         iMon = atol(acTmp);
         pTmp = strchr(acTmp, '/');
         if (pTmp)
         {
            pTmp++;
            iDate = atol(pTmp);
            pRet = strchr(pTmp, '/');
            if (pRet)
            {
               pRet++;
               iYear = atoi(pRet);
               if (iYear < 100)
               {
                  iYear += 2000;
                  if (iYear > iCurYear)
                     iYear -= 100;
               } else if (iYear < 1000)
               {
                  *(pRet+2) = 0;
                  iYear = atoi(pRet);
                  iYear += 2000;
                  if (iYear > iCurYear)
                     iYear -= 100;
               }
               sprintf(pToDate, "%d%0.2d%0.2d", iYear, iMon, iDate);
               pRet = pToDate;
            }
         }
         break;
   }

   if (pRet && strlen(pRet) > 8)
      *(pRet + 8) = 0;

   if (pRet && (iFromFmt != YY2YYYY) && !isValidYMD(pRet))
      pRet = NULL;

   return pRet;
}

/*****************************************************************************
 *
 * Remove $ and comma from a number
 * Return string length
 *
 *****************************************************************************/

int dollar2Num(char *pDollar, char *pNumStr)
{
   int iRet = 0;

   *pNumStr = 0;
   while (*pDollar)
   {
      if (*pDollar >= '.' &&  *pDollar <= '9')
      {
         iRet++;
         *pNumStr++ = *pDollar;
      }
      pDollar++;
   }
   *pNumStr = 0;
   return iRet;
}

/*****************************************************************************/

int csv2fix(char *pInfile, char *pOutfile, char *pRecDef)
{
   return 0;
}

/*****************************************************************************/

// Delete file(s), wild card accepted
// Return 0 if successful, -1 if file not exist
int delFile(char *pFilename)
{
   long     lHandle;
   int      iTmp, iRet=0;
   char     acTmp[256], acPath[256], *pTmp;
   struct   _finddata_t  sFileInfo;

   lHandle = _findfirst(pFilename, &sFileInfo);
   if (lHandle <= 0)
      return -1;

   // Store path name
   strcpy(acPath, pFilename);
   pTmp = strrchr(acPath, '\\');
   if (pTmp)
   {
      *(++pTmp) = 0;
      iTmp = 0;
      while (!iTmp)
      {
         sprintf(acTmp, "%s%s", acPath, sFileInfo.name);
         iRet |= remove(acTmp);

         // Find next file
         iTmp = _findnext(lHandle, &sFileInfo);
      }
   } else
   {
      remove(pFilename);
   }

   // Close handle
   _findclose(lHandle);

   return iRet;
}

/*****************************************************************************/

bool isValidMDY(char *pDate)
{
   bool bRet=true;
   int  iTmp;

   // Validate year
   iTmp = atoin(pDate+4, 4);
   if (iTmp < 1900 || iTmp > 2027)
      bRet = false;
   else
   {
      // Validate month
      iTmp = atoin(pDate, 2);
      if (iTmp < 1 || iTmp > 12)
         bRet = false;
      else
      {
         // Validate date
         iTmp = atoin(pDate+2, 2);
         if (iTmp < 1 || iTmp > 31)
            bRet = false;
      }
   }

   return bRet;
}

/*****************************************************************************/

LPSTR isCharIncluded(LPSTR pBuf, char cChar, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (*pBuf == cChar)
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

LPSTR isCharIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (isalpha(*pBuf))
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

LPSTR isTabIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (*pBuf == '\t')
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

// Return position of digit starts
LPSTR isNumIncluded(LPSTR pBuf, int iLen)
{
   int   iRet, iCnt;
   LPSTR pRet=NULL;

   if (iLen > 0)
      iCnt = iLen;
   else
      iCnt = strlen(pBuf);

   for (iRet = 0; iRet < iCnt; iRet++)
   {
      if (isdigit(*pBuf))
      {
         pRet = pBuf;
         break;
      }
      pBuf++;
   }
   return pRet;
}

/*****************************************************************************/

void DoValidDate(LPSTR pBuf, int iLen)
{
   int iRet;

   for (iRet = 0; iRet < iLen; iRet++)
   {
      if (*pBuf < '0' || *pBuf > '9')
         *pBuf = '0';
      pBuf++;
   }
}

/***************************************************************************
 *
 * Determines whether the year is a leap year
 *
 ***************************************************************************/

bool isLeapYear(long year)
{
   if ( ((year % 4) == 0) &&
        (((year % 100) != 0) || ((year % 400) == 0)))
   {
      return true;
   } else
   {
      return false;
   }
}

/***************************************************************************
 * Function: gdate_daytomy
 *
 * Purpose:
 *
 * converts day to d/m/y
 ***************************************************************************/

void gdate_daytodmy(long days, int *yrp, int *monthp, int *dayp)
{
   int years;
   int nleaps;
   int month;
   int mdays;

   /* get number of completed years and calc leap days */
   years = (int) (days / 365);
   days = days % 365;
   nleaps = (years / 4) - (years / 100) + (years / 400);

   while (nleaps > days)
   {
      days += 365;
      years--;
      nleaps = (years / 4) - (years / 100) + (years / 400);
   }

   days -= nleaps;

   /* add one year for current (non-complete) year */
   years++;

   /* current month */
   for (month = 0; month < 12; month++)
   {
      mdays = monthdays[month];
      if (isLeapYear(years) && (month == 1))
      {
         mdays++;
      }

      if (days == mdays)
      {
         days = 0;
         month++;
         break;
      } else if (days < mdays)
      {
         break;
      } else
      {
         days -= mdays;
      }
   }
   /* conv month from 0-11 to 1-12 */

   if (monthp != NULL)
   {
      *monthp = month+1;
   }

   if (dayp != NULL)
   {
      *dayp = (int) days + 1;
   }

   if (yrp != NULL)
   {
      *yrp = years;
   }
}

/***************************************************************************
 * Function: gdate_dmytoday
 *
 * Purpose:
 *
 * converts d/m/y to a day
 ***************************************************************************/

long gdate_dmytoday(int yr, int month, int day)
{
   int nleaps;
   int i;
   long ndays;

   /* exclude the current year */
   yr--;
   nleaps = (yr / 4) - (yr / 100) + (yr / 400);

   /* in any given year, day 0 is jan1 */
   month--;
   day--;
   ndays = 0;

   for (i = 0; i < month ; i++)
   {
      ndays += monthdays[i];
      if (isLeapYear(yr+1) && (i == 1))
      {
         ndays++;
      }
   }

   ndays = ndays + day + nleaps + (yr * 365L);
   return(ndays);
}

/***************************************************************************
 * Function: gdate_monthdays
 *
 * Purpose:
 *
 * Gets number of days in month
 ***************************************************************************/

int gdate_monthdays(int month, int year)
{
   int ndays;
   ndays = monthdays[month - 1];

   if (isLeapYear(year) && (month == 2))
   {
      ndays++;
   }

   return(ndays);
}

/***************************************************************************
 * Function: gdate_weekday
 *
 * Purpose:
 *
 * Gets the day of the week
 ***************************************************************************/

int gdate_weekday(long daynr)
{
   return((int) ((daynr + 1) % 7));
}

// Create a Julian date of the form YYYYDDD
// from a Gregorian date YYYYMMDD
long GregorianToJulian(LPCSTR pGDate, LPSTR pJDate)
{
   long JDate;
   long iMonth, iDay, iYear;

   iMonth = atoin((char *)(pGDate+4), 2);
   iDay   = atoin((char *)(pGDate+6), 2);
   iYear  = atoin((char *)pGDate, 4);
   JDate = 0;

   // Add in days for months already elapsed.
   for( int i = 0; i < iMonth - 1; ++i )
      JDate += monthdays[i];

   // Add in days for this month.
   JDate += iDay;

   // Check for leap year.
   if ((iYear % 100) != 0 && (iYear % 4) == 0 )
      JDate++;

   // Add in year.
   JDate += iYear*1000;
   if (pJDate)
      sprintf(pJDate, "%ld", JDate);

   return JDate;
}

// Convert CCYYDDD to YYYYMMDD
long JulianToGregorian(LPCSTR pJDate, LPSTR pGDate)
{
   long  JDate;
   long  iMonth, iDay, iYear, lRet;

   JDate = atoin((char *)(pJDate+4), 3);
   if (!JDate)
      return 0;

   iYear = atoin((char *)pJDate, 4);
   for (iMonth=0, iDay=JDate; iMonth < 12; iMonth++)
   {
      if (iDay <= monthdays[iMonth])
         break;

      if (iMonth == 1 && isLeapYear(iYear))
      {
         if (iDay == monthdays[iMonth]+1)
            break;

         iDay--;
      }

      iDay -= monthdays[iMonth];
   }

   iMonth++;
   lRet = iYear*10000 + iMonth*100 + iDay;
   if (pGDate)
      sprintf(pGDate, "%ld", lRet);

   return lRet;
}

/*****************************************************************************
 *
 * GetNextMonth()
 *
 ****************************************************************************/

void getNextMonth(char *pBuf, int iMonths)
{
   SYSTEMTIME  curSystemTime;

   GetLocalTime(&curSystemTime);

   if (curSystemTime.wMonth >= 12)
   {
      curSystemTime.wMonth = 1;
      curSystemTime.wYear += 1;
   } else
      curSystemTime.wMonth += 1;

   // Calculate leap year
   if (curSystemTime.wMonth == 2 && curSystemTime.wDay > 28)
   {
      if (isLeapYear(curSystemTime.wYear))
         curSystemTime.wDay = 29;
      else
         curSystemTime.wDay = 28;
   }

   sprintf(pBuf, "%.4d%.2d%.2d", curSystemTime.wYear, curSystemTime.wMonth, curSystemTime.wDay);
}

/*****************************************************************************
 *
 *
 ****************************************************************************/

void getPrevMonth(char *pBuf, int iMonths)
{
   SYSTEMTIME  curSystemTime;

   GetLocalTime(&curSystemTime);

   if (iMonths > 11)
   {
      *pBuf = 0;
      return;
   }

   if (curSystemTime.wMonth <= iMonths)
   {
      curSystemTime.wMonth = (curSystemTime.wMonth+12)-iMonths;
      curSystemTime.wYear -= 1;
   } else
      curSystemTime.wMonth -= iMonths;

   // Calculate leap year
   if (curSystemTime.wMonth == 2 && curSystemTime.wDay > 28)
   {
      if (isLeapYear(curSystemTime.wYear))
         curSystemTime.wDay = 29;
      else
         curSystemTime.wDay = 28;
   }

   sprintf(pBuf, "%.4d%.2d%.2d", curSystemTime.wYear, curSystemTime.wMonth, curSystemTime.wDay);
}

/*****************************************************************************
 *
 * Move GrGr files to its subfolder.
 * Return:
 *    -1 : if error
 *     0 : file not available
 *     1 : successfully move
 *
 ****************************************************************************/

int moveGrGrFiles(LPCSTR pIniFile, LPCSTR pCnty, bool bAddDate)
{
   char     acGrGrSrc[_MAX_PATH], acTmp[_MAX_PATH];
   char     acSrc[_MAX_PATH], acDst[_MAX_PATH], acDate[32], *pTmp;
   long     lHandle;
   int      iRet;
   struct   _finddata_t  sFileInfo;

   GetIniString(pCnty, "GrGrSrc", "", acGrGrSrc, _MAX_PATH, pIniFile);
   // Check for file exist
   lHandle = _findfirst(acGrGrSrc, &sFileInfo);
   if (lHandle > 0)
   {
      strcpy(acTmp, acGrGrSrc);
      pTmp = strrchr(acGrGrSrc, '\\');
      if (pTmp)
         *++pTmp = 0;
      else
      {
         _findclose(lHandle);
         return -1;
      }

      // Get today date
      dateString(acDate, 0);

      do
      {
         sprintf(acSrc, "%s%s", acGrGrSrc, sFileInfo.name);
         if (!bAddDate)
            sprintf(acDst, "%sGrGr\\%s", acGrGrSrc, sFileInfo.name);
         else
         {
            strcpy(acTmp, sFileInfo.name);
            pTmp = strrchr(acTmp, '.');
            if (pTmp)
            {
               *pTmp++ = 0;
               sprintf(acDst, "%sGrGr\\%s_%s.%s", acGrGrSrc, acTmp, acDate, pTmp);
            } else
               sprintf(acDst, "%sGrGr\\%s_%s", acGrGrSrc, acTmp, acDate);
         }

         // Remove old file if exist
         if (!_access(acDst, 0))
            remove(acDst);

         // Move file
         iRet = rename(acSrc, acDst);
         if (iRet == -1)
         {
            iRet = -1234;
            break;
         }

         // Find next file
         iRet = _findnext(lHandle, &sFileInfo);
      } while (!iRet);

      // Close handle
      _findclose(lHandle);

      if (iRet != -1234)
         iRet = 1;
   } else
      iRet = 0;

   return iRet;
}

/*****************************************************************************
 *
 * Move GrGr files to its subfolder.
 *
 ****************************************************************************/

void fixDec(char *pStr, int iPos)
{
   char acTmp[32], *pTmp;

   pTmp = pStr;
   while (*pTmp == ' ')
      pTmp++;
   memcpy(acTmp, pTmp, iPos);
   acTmp[iPos] = '.';
   strcpy((char *)&acTmp[iPos+1], pTmp+iPos);
   strcpy(pStr, acTmp);
}

/*****************************************************************************
 *
 * Set file time
 *
 ****************************************************************************/

void touch(char *pFilename)
{
   HANDLE      hFile;
   FILETIME    ft;
   SYSTEMTIME  st;

   hFile = CreateFile(pFilename, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   GetSystemTime(&st);                 // gets current time
   SystemTimeToFileTime(&st, &ft);     // converts to file time format
   SetFileTime(hFile, &ft, &ft, &ft);  // sets last-write time for file
   CloseHandle(hFile);
}

/********************************* isTextFile *******************************
 *
 * Test first 2048 bytes for CR or LF.  Return TRUE if it seems to be text file
 * with CRLF.  FALSE if no CRLF or NULL character found.
 *
 *****************************************************************************/

bool isTextFile(LPCSTR pInfile, int iLineLen)
{
   int      iTmp, iLen;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[8192];
   bool     bRet = false;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return false;

   iLen = iLineLen;
   if (iLen > 8192)
      iLen = 8192;

   if (ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL))
   {
      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == 13 || acBuf[iTmp] == 10)
         {
            bRet = true;
            break;
         } else if (!acBuf[iTmp] || acBuf[iTmp] > 127)
            break;

         iTmp++;
      }
   }

   if (fhIn)
      CloseHandle(fhIn);

   return bRet;
}

/******************************* isTabEmbeded ********************************
 *
 * Test first 2048 bytes for tab character.  Return TRUE if tab is found.
 * FALSE if not
 *
 *****************************************************************************/

bool isTabEmbeded(LPCSTR pInfile)
{
   int      iTmp;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[2048];
   bool     bRet = false;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return false;

   bRet = false;
   if (ReadFile(fhIn, acBuf, 2048, &nBytesRead, NULL))
   {
      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         if (acBuf[iTmp] == 9)
         {
            bRet = true;
            break;
         } else if (acBuf[iTmp] == 13 || acBuf[iTmp] == 10)
            break;
         iTmp++;
      }
   }

   if (fhIn)
      CloseHandle(fhIn);

   return bRet;
}

/********************************* guessRecLen *******************************
 *
 * Test first 8192 bytes for CR or LF.  If there is LF after expected line length,
 * return iLineLen+1.  If CRLF found, return iLineLen+2, else return iLineLen.
 *
 *****************************************************************************/

int guessRecLen(LPCSTR pInfile, int iLineLen)
{
   int      iRet, iLen;
   unsigned long   nBytesRead;
   HANDLE   fhIn;
   char     acBuf[8192];

   if (!iLineLen)
      return 0;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return 0;

   iLen = iLineLen;
   if (iLen > 8192)
      iLen = 8192;

   iRet = iLineLen;
   if (ReadFile(fhIn, acBuf, iLen+2, &nBytesRead, NULL))
   {
      if (acBuf[iLineLen] == 10)
         iRet = iLineLen+1;
      else if (acBuf[iLineLen+1] == 10)
         iRet = iLineLen+2;
   }

   CloseHandle(fhIn);

   return iRet;
}

/****************************** AnsiToUnicode *********************************
 *
 * AnsiToUnicode converts the ANSI string pszA to a Unicode string
 * and returns the Unicode string through ppszW. Space for the
 * the converted string is allocated by AnsiToUnicode.
 *
 ******************************************************************************/

HRESULT __stdcall AnsiToUnicode(LPCSTR pszA, wchar_t *pszW, int iLen) 
{ 
   int cCharacters;
   DWORD dwError;

   *pszW = 0;

   // If input is null then just return the same.
   if (NULL == pszA)
      return NOERROR;

   // Determine number of wide characters to be allocated for the
   // Unicode string.
   cCharacters =  strlen(pszA);
   if (cCharacters > iLen)
      cCharacters = iLen;

   // Covert to Unicode.
   if (0 == MultiByteToWideChar(CP_ACP, 0, pszA, cCharacters, pszW, cCharacters))
   {
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }

   *(pszW+cCharacters) = 0;
   return NOERROR;
} 

/****************************** UnicodeToAnsi *********************************
 *
 * UnicodeToAnsi converts the Unicode string pszW to an ANSI string
 * and returns the ANSI string through ppszA. Space for the
 * the converted string is allocated by UnicodeToAnsi.
 *
 ******************************************************************************/

HRESULT __stdcall UnicodeToAnsi(wchar_t *pszW, LPSTR pszA, int iLen) 
{ 
   int   iChars;
   DWORD dwError;

   *pszA = 0;

   // If input is null then just return the same.
   if (pszW == NULL)
      return NOERROR;

   iChars = wcslen(pszW);
   if (iChars > iLen)
      iChars = iLen-1;

   // Convert to ANSI.
   if (0 == WideCharToMultiByte(CP_ACP, 0, pszW, iChars, pszA, iChars, NULL, NULL))
   {
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }

   *(pszA+iChars) = 0;
   return NOERROR;
}

/****************************** UnicodeToAnsi *********************************
 *
 * Convert the Windows Unicode file to ANSI text file.
 * 08/24/2011 Add code to check for UTF-8 format (VEN).
 * 09/15/2011 Fix bug that report error even though it is ok.
 *
 ******************************************************************************/

HRESULT __stdcall UnicodeToAnsi(LPCSTR pInfile, LPCSTR pOutfile) 
{ 
   CString  csText;
   CFile    cfInfile, cfOutfile;
   int      nFileLen;
   DWORD    dwError;

   LogMsgD("Converting Unicode to Ansi: %s", pInfile);
   if (!cfInfile.Open( pInfile, CFile::modeRead ) )
   {
      LogMsg("***** Error unable to open file: %s", pInfile);
      dwError = GetLastError();
      return HRESULT_FROM_WIN32(dwError);
   }
   nFileLen = (int)cfInfile.GetLength();

   // Allocate buffer for binary file data
   unsigned char* pBuffer = new unsigned char[nFileLen + 2];
   cfInfile.Read(pBuffer, 2);

   // Windows Unicode file is detected if starts with FEFF
   if (pBuffer[0] == 0xff && pBuffer[1] == 0xfe )
   {
      nFileLen -= 2;
      nFileLen = cfInfile.Read(pBuffer, nFileLen);
      pBuffer[nFileLen] = '\0';
      pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded
      // Contains byte order mark, so assume wide char content
      // non _UNICODE builds should perform UCS-2 (wide char) to UTF-8 conversion here
      csText = (LPCWSTR)(&pBuffer[0]);

      try
      {
         cfOutfile.Open(pOutfile, CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive);
         cfOutfile.Write(csText, csText.GetLength());
         cfOutfile.Close();
         dwError = 0;
      }
      catch (CFileException e)
      {
         LogMsg("***** Error unable to create file: %s (%d)", pOutfile, e.m_cause);
         dwError = GetLastError();
      }
   } else if (pBuffer[0] == 0xef && pBuffer[1] == 0xbb )
   {
      // Throw away 3rd byte 0xbf
      cfInfile.Read(pBuffer, 1);
      nFileLen -= 3;
      nFileLen = cfInfile.Read(pBuffer, nFileLen);
      pBuffer[nFileLen] = '\0';
      pBuffer[nFileLen+1] = '\0'; // in case 2-byte encoded
      csText = (LPCSTR)(&pBuffer[0]);

      try
      {
         cfOutfile.Open(pOutfile, CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive);
         cfOutfile.Write(csText, csText.GetLength());
         cfOutfile.Close();
         dwError = 0;
      }
      catch (CFileException e)
      {
         LogMsg("***** Error unable to create file: %s (%d)", pOutfile, e.m_cause);
         dwError = GetLastError();
      }
   } else
   {
      LogMsg("*** %s is not a Windows Unicode file.  No conversion.", pInfile);
      dwError = 1;
   }

   cfInfile.Close();
   delete [] pBuffer;

   return dwError;
}

/*****************************************************************************/

void memsetU(wchar_t *var, wchar_t wChar, int iLen)
{
   int   iCnt;
   for (iCnt = 0; iCnt < iLen; iCnt++)
      *var++ = wChar;
   *var = 0;
}

/*****************************************************************************/

void __stdcall GetCharW(wchar_t *var, LPSTR str, int varLen) 
{
   sprintf(str, "%.*S", varLen, var);
}

/*****************************************************************************/

void __stdcall SetCharW(wchar_t *var, LPSTR str, int varLen) 
{
   int iCnt;

   iCnt = swprintf(var, L"%.*S", varLen, str);
   if (varLen > iCnt) 
      memsetU(var+iCnt, L' ', varLen-iCnt);
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for even number of double quote.
 *       If not, keep reading.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int countQuotes(char *pBuf)
{
   int iRet = 0;

   while (*pBuf)
      if (*pBuf++ == '"')
         iRet++;

   return iRet;
}

int myGetStrEQ(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            // Remove LF to avoid break in the middle
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = countQuotes(pBuf);
            if ((iRet % 2) == 0)
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for double quote as last 
 *       character.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrQC(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            if (sTmp[iTmp-1] == '"')
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for double quote as last 
 *       character and previous char is not a separator.  It also check for
 *       null character embeded in string and replace it with space.
 *       Use this special version only when you know how it works.  Use in
 *       MergeMno.cpp
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrQC(LPSTR pBuf, char cSeparator, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
#ifdef _DEBUG
         //if (!memcmp(&sTmp[1], "X0287319", 8) )
         //   iTmp = 0;
#endif
         // Remove null char in string
         for (iTmp=0; sTmp[iTmp] != '\n'; iTmp++)
         {
            if (!sTmp[iTmp])
               sTmp[iTmp] = ' ';
         }

         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
               iTmp--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            // Avoid line ending with incomplete token
            if (sTmp[iTmp-1] == '"' && sTmp[iTmp-2] != cSeparator)
               bDone = true;
            // Or invalid token as in ""B""
            if (sTmp[iTmp-1] == '"' && sTmp[iTmp-2] == '"' && sTmp[iTmp-3] != cSeparator)
               bDone = false;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for digit as last character.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrDC(char *pBuf, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = replNull(sTmp);
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
            if (isdigit(sTmp[iRet-1]))
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for number of token count.
 *       Read until get count tokens.
 *
 * 09/23/2015: call countChar() if delimiter is '|'
 * 10/16/2015: Fix bug by setting #tokens = countChar()+1 (SCR, BUT & SIS might affect the change)
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *    -3: Number of tokens read > iCount
 *
 *****************************************************************************/

int myGetStrTC(char *pBuf, unsigned char cDelimiter, int iCount, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false;
   int   iRet, iTmp, iLen;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      if (fgets(sTmp, 2048, fd))
      {
         iTmp = replNull(sTmp);
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            if (cDelimiter == '|')
               iRet = countChar(pBuf, cDelimiter)+1;
            else
               iRet = countTokens(pBuf, cDelimiter);
            if (iRet >= iCount)
               bDone = true;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

typedef wchar_t      FLD_NAME[20];
//typedef struct {
//   wchar_t name[20];
//   wchar_t type[1];
//   wchar_t value[50];
//} ZPTGFNFD;

#define sizeofU(var) sizeof(var)/2

void Get_Match_Docs(FLD_NAME *iFld_name1)
{
/*
   char     fld_name_buf1[21];
   int iTmp;
   iTmp = sizeofU(*iFld_name1);
   iTmp = sizeofU(iFld_name1);
   iTmp = sizeofU(FLD_NAME);
   iTmp = sizeof(ZPTGFNFD);
   iTmp = sizeofU(ZPTGFNFD);

   GetCharW(*iFld_name1, fld_name_buf1, sizeofU(*iFld_name1));
*/
}

void doTest(wchar_t *pszW, LPSTR pszA)
{
   /*
   HRESULT  lRet;
   int      iTmp, iSize;
   char     ansiBuf[19];
   wchar_t  wideBuf[50];
   FLD_NAME iFld_name1;

   wcscpy(iFld_name1, L"Name Test");
   Get_Match_Docs(&iFld_name1);

   iTmp = strlen(pszA);
   if (iTmp > 0)
   {
      // Convert ANSI to Wide character
      //lRet = AnsiToUnicode(pszA, wideBuf, iTmp);

      // Test SetCharW
      iSize = sizeof(wideBuf)/2;
      SetCharW(wideBuf, pszA, iSize);
      wcscpy(pszW, wideBuf);
   } else
   {
      iTmp = wcslen(pszW);
      if (iTmp > 0)
      {
         // Convert Wide character to ANSI
         //lRet = UnicodeToAnsi(pszW, ansiBuf, 100);
         iSize = sizeof(ansiBuf);
         GetCharW(pszW, ansiBuf, iSize);
         strcpy(pszA, ansiBuf);
      }
   }
   */
}

/******************************** mkpath() *********************************
 *
 * Creating full path folder.  This only works on local or map drive.  Drive 
 * letter is required.
 *
 ***************************************************************************/

int mkpath(char *pPath)
{
   char  *pTmp, *pTmp1, sPath[_MAX_PATH];
   int iRet=0;

   if (*(pPath+1) != ':')
      return -1;

   strcpy(sPath, pPath);
   pTmp1 = sPath;
   while (!iRet && (pTmp = strchr(pTmp1, '\\')))
   {
      *pTmp = 0;
      if (_access(sPath, 0))
         iRet = _mkdir(sPath);
      *pTmp = '\\';
      pTmp1 = ++pTmp;
   }

   if ((sPath[strlen(sPath)-1] != '\\') && _access(sPath, 0))
      iRet = _mkdir(sPath);

   return iRet;
}

/****************************** getFileDate() ******************************
 *
 * Return last modified file date
 *
 ***************************************************************************/

long getFileDate(LPCSTR pFilename, LPSTR timeStr)
{
   __int64  iTmp;
   long     lRet=0;
   char     acTmp[64];
   struct   _stati64 myStat;


   if (timeStr)
      *timeStr = 0;
   if (!_access(pFilename, 0))
   {
      iTmp = _stati64(pFilename, &myStat);
      dateString(acTmp, myStat.st_mtime);
      lRet = atol(acTmp);

      if (timeStr)
         strcpy(timeStr, acTmp);
   }

   return lRet;
}

/****************************** getFileSize() ******************************
 *
 * Return file size
 *
 ***************************************************************************/

__int64 getFileSize(LPCSTR pFilename)
{
   __int64   iRet=0;
   struct _stati64 sResult;

   if (!_access(pFilename, 0))
   {
      iRet = _stati64(pFilename, &sResult);
      if (!iRet)
         iRet = sResult.st_size;
   }
   return iRet;
}

/****************************** replEmbededChar ******************************
 *
 * Check file for null char embeded.  If output file provided, create output file.
 * Mode: -1=LT, 0=EQ, 1=GT
 *
 * Return 1 if char found, 0 if not.  Negative value if error occurs
 *
 *****************************************************************************/

int replEmbededChar(LPCSTR pInfile, LPCSTR pOutfile, char bSrchChar, char bReplChar, int iMode, int iRecLen)
{
   int      iTmp, iRet, iErrAt;
   long     lCnt=0;
   unsigned long   nBytesWritten, nBytesRead, iLen;
   BOOL     bRet;
   HANDLE   fhIn, fhOut;
   char     acBuf[8192];

   if (iRecLen <= 0 || iRecLen > 8192)
      iLen = 8192;
   else
      iLen = iRecLen;

   // Create output file if file name provided
   if (pOutfile && !_access(pOutfile, 0))
   {
      // Open Output file
      fhOut = CreateFile(pOutfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

      if (fhOut == INVALID_HANDLE_VALUE)
         return -2;
   } else
      fhOut = 0;

   // Open input file
   fhIn = CreateFile(pInfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
      return -1;

   iRet = 0;
   iErrAt = -1;
   bRet = true;
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iLen, &nBytesRead, NULL);
      if (!bRet || !nBytesRead)
         break;

      iTmp = 0;
      while (iTmp < nBytesRead)
      {
         switch(iMode)
         {
            case 0:
               bRet = (acBuf[iTmp] == bSrchChar);
               break;
            case 1:
               bRet = (acBuf[iTmp] > bSrchChar);
               break;
            default:
               bRet = (acBuf[iTmp] < bSrchChar);
               break;
         }
         if (bRet)
         {
            iRet = 1;      
            if (fhOut)
            {
               if (iErrAt != lCnt)
               {
                  iErrAt = lCnt;
                  LogMsgD("***** Char <0x%2X> found at %d row %d", (unsigned char)acBuf[iTmp], iTmp, lCnt);
               }
               acBuf[iTmp] = bReplChar;
            } else
            {
               LogMsgD("***** Char <0x%2X> found at %d row %d", (unsigned char)acBuf[iTmp], iTmp, lCnt);
               break;
            }
         }
         iTmp++;
      }

      if (fhOut)
         bRet = WriteFile(fhOut, acBuf, nBytesRead, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   return iRet;
}

/*****************************************************************************
 *
 * Desc: Get a string from a text file and check for special first character and
 *       digit as the second byte to start a record.
 *
 * Return number of bytes read.  <0 if read error
 *     0: EOF
 *    -1: File not open, invalid buffer pointer, invalid max buffer length
 *    -2: Buffer is not big enough
 *
 *****************************************************************************/

int myGetStrBD(char *pBuf, char bChar, int iBufLen, FILE *fd)
{
   char  sTmp[2048];
   bool  bDone=false, bStart=true;
   int   iRet, iTmp, iLen;
   long  lCurPos;

   if (!fd || !pBuf || iBufLen <= 0)
      return -1;

   *pBuf = 0;
   iLen = 0;
   do 
   {
      lCurPos = ftell(fd);
      if (fgets(sTmp, 2048, fd))
      {
         if (!bStart && (sTmp[0] == bChar) && isdigit(sTmp[1]))
         {
            bDone = true;
            fseek(fd, lCurPos, SEEK_SET);
         }

         bStart = false;
         iTmp = strlen(sTmp);
         iLen += iTmp;     // New output len
         if (iLen > iBufLen)
         {
            iRet = -2;
            bDone = true;
         } else
         {
            if (sTmp[iTmp-1] == '\n')
            {
               sTmp[iTmp-1] = 0;
               iLen--;
            }

            strcat(pBuf, sTmp);
            iRet = iLen;
         }
      } else
      {
         bDone = true;
         iRet = 0;
      }
   } while (!bDone);

   return iRet;
}

/****************************************************************************
 *
 * Count number of records having the same date as specified
 *
 * Type: 1=year, 2=yyyymm, 3=yyyymmdd
 *
 ****************************************************************************/

int countDate(LPSTR pFilename, long lDate, long lOffset, long lType)
{
   FILE *fd;
   char  acRec[1024], *pTmp;
   long  lCnt, lTmp, lDone;

   lDone = lCnt = 0;
   fd = fopen(pFilename, "r");
   if (fd)
   {
      while (pTmp = fgets(acRec, 1024, fd))
      {
         if (lType == 1)
            lTmp = atoin(&acRec[lOffset], 4);
         else if (lType == 2)
            lTmp = atoin(&acRec[lOffset], 6);
         else if (lType == 3)
            lTmp = atoin(&acRec[lOffset], 8);

         if (lTmp == lDate)
            lDone++;

         if (lTmp > 2008 && acRec[lOffset+5] >= '5' && acRec[15] < '2')
            lTmp = 0;

         if (!(++lCnt % 10000))
            printf("\r%u", lCnt);
      }
   }

   return lDone;
}

/****************************************************************************
 *
 * Count number of token of a string.  Return number of tokens.
 *
 ****************************************************************************/

int countTokens(LPSTR pString, char cDelimiter)
{
   int i;
   int j = 1;
   int k;
   int InsideQuotes = 0;

   rTrim(pString);
   k = (int)strlen(pString);

   for (i=0; i<k; i++)
   {                
      if (pString[i] == '\0' || pString[i] == 10)
         break;
      if (pString[i] == '\"')
         InsideQuotes = (InsideQuotes+1) % 2;
      if (InsideQuotes)
         continue;
      if (pString[i] == cDelimiter)
         j++;
   }

   return j;
}

/****************************************************************************
 *
 * Count number of token of specified record in a file
 *
 ****************************************************************************/

int countToken(LPCSTR pFilename, int iRecNum, char cDelimiter)
{
   FILE *fd;
   char  acRec[4096], *pTmp;
   int   iTmp, iCnt=-1;

   fd = fopen(pFilename, "r");
   if (fd)
   {
      for (iTmp = 0; iTmp < iRecNum; iTmp++)
      {
         pTmp = fgets(acRec, 4096, fd);
         if (!pTmp)
            break;
      }
      
      if (iTmp == iRecNum)
         iCnt = countChar(acRec, cDelimiter);

      fclose(fd);
   }

   return iCnt+1;
}

/****************************************************************************
 *
 * Reset a character in the file for every line at specific place
 * Output the result to *.out
 *
 ****************************************************************************/

int resetOneChar(LPSTR pInfile, long lOffset, char bRepl)
{
   FILE *fdIn, *fdOut;
   char  sInrec[1024], sOutfile[_MAX_PATH], *pTmp;
   long  lCnt;

   strcpy(sOutfile, pInfile);
   pTmp = strrchr(sOutfile, '.');
   strcpy(pTmp, ".out");

   lCnt = 0;
   fdIn = fopen(pInfile, "r");
   fdOut = fopen(sOutfile, "w");
   if (fdIn)
   {
      fdOut = fopen(sOutfile, "w");
      while (pTmp = fgets(sInrec, 1024, fdIn))
      {
         sInrec[lOffset] = '0';
         fputs(sInrec, fdOut);

         if (!(++lCnt % 10000))
            printf("\r%u", lCnt);
      }

      fclose(fdOut);
      fclose(fdIn);
   }

   return lCnt;
}

/****************************************************************************
 *
 * Count number of occurences of a char in the string
 *
 ****************************************************************************/

int countChar(LPCSTR pBuf, char bChar)
{
   int iRet = 0;

   while (*pBuf)
      if (*pBuf++ == bChar)
         iRet++;

   return iRet;
}

/*****************************************************************************
 *
 * Get specified date based on today.  iDays > for future date and iDays < 0 for past date
 *
 ****************************************************************************/

int getExtendDate(char *pDate, int iDays)
{
   CTime today = CTime::GetCurrentTime();
   CTimeSpan tsExtend(iDays,0,0,0);
   today += tsExtend;

   if (pDate)
      strcpy(pDate, today.Format("%Y%m%d"));

   return atoi(today.Format("%Y%m%d"));
}

/*****************************************************************************
 *
 * Convert a word or a sentence to proper case.
 *
 ****************************************************************************/

CString toProperCase(CString& sWord)
{
   CString  sTmp;

   sTmp = sWord;
   sTmp.MakeLower();
   toupper(sTmp.GetAt(0));      

   return sTmp;
}

char *toProperCase(char *pWord, int iLen)
{
   char  *pTmp, *pResult;
   int   iTmp, iIdx;

   if (iLen > 0)
      iTmp = iLen;
   else
      iTmp = strlen(pWord);

   pTmp = pWord;
   pResult = pWord;
   *pResult++ = _toupper(*pTmp++);
   for (iIdx = 1; iIdx < iTmp; iIdx++)
      *pResult++ = _tolower(*pTmp++);

   return pWord;
}

/**************************************************************************
 *
 **************************************************************************/

int replBadChars(char* pInfile, char *pOutfile, int iLen)
{
   FILE *fdInput, *fdOutput;
   unsigned char acTmp[8192];
   int  iRet, iTmp, iCnt=0, iRecLen, iWriteLen, iFound=0;

   if (iLen > 0)
      iRecLen = iLen;
   else
      iRecLen = 8192;

   LogMsg("Check for bad chars in %s", pInfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "rb");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "wb");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }

   // Start process
   while (!feof(fdInput))
   {
      iRet = fread(acTmp, 1, iRecLen, fdInput);
      if (!iRet)
         break;

      iWriteLen = iRet;

      // Ignore CRLF
      if (acTmp[iRet-1] == 10 || acTmp[iRet-1] == 13)
      {
         iRet--;
         if (acTmp[iRet-1] == 13)
            iRet--;
      }

      for (iTmp = 0; iTmp < iRet; iTmp++)
      {
         if (acTmp[iTmp] < ' ' || acTmp[iTmp] > '~')
         {
            if (acTmp[iTmp] == 0)
               LogMsg0("Null char is found at record %d offset %d [%.16s]", iCnt, iTmp+1, acTmp);
            else
               LogMsg0("Bad char (%d) is found at record %d offset %d [%.16s]", acTmp[iTmp], iCnt, iTmp+1, acTmp);
            acTmp[iTmp] = 0x20;
            iFound++;
         }
      }

      // Wrire data to output file
      if (fdOutput)
         iRet = fwrite(acTmp, 1, iWriteLen, fdOutput);

      if (!(++iCnt%1000))
         printf("\r%ld", iCnt);
   }

   fclose(fdInput);
   if (fdOutput)
      fclose(fdOutput);

   printf("\n");
   LogMsg("Total record processed: %d", iCnt);
   LogMsg("Bad chars found:        %d\n", iFound);
   return iFound;
}

/*****************************************************************************
 *
 * Create an empty flag file
 *
 *****************************************************************************/

int CreateFlgFile(char *pCnty, char *pIniFile)
{
   FILE  *fd;
   char  sFlgFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet;

   GetIniString("Data", "FlgFile", "", sTmp, _MAX_PATH, pIniFile);
   sprintf(sFlgFile, sTmp, pCnty, pCnty);
   fd = fopen(sFlgFile, "w");
   if (fd)
   {
      iRet = 0;
      fclose(fd);
   } else
      iRet = -1;

   return iRet;
}

/*****************************************************************************
 *
 * Remove all normal files in temp folder
 *
 *****************************************************************************/

int RemoveTempFiles(char *pCnty, char *pTmpPath, bool bDelZip)
{
   char  sTmpFile[_MAX_PATH], sTmp[_MAX_PATH];
   int   iRet, iErrno=0;
   long  lHandle;
   struct _finddata_t sFileInfo;

   LogMsg("Remove temp files ...");
   sprintf(sTmp, "%s\\%s\\*.*", pTmpPath, pCnty);
   sFileInfo.attrib = _A_NORMAL;
   lHandle = _findfirst(sTmp, &sFileInfo);
   if (lHandle > 0)
   {
      do
      {
         if (sFileInfo.name[0] != '.' && (!bDelZip || !strstr(sFileInfo.name, ".zip")))
         {
            sprintf(sTmpFile, "%s\\%s\\%s", pTmpPath, pCnty, sFileInfo.name);
            iRet = sFileInfo.attrib & 0x00FF;
            if (iRet == _A_NORMAL || iRet == _A_ARCH)
            {
               LogMsg("--> Removing %s", sTmpFile);
               if (!DeleteFile(sTmpFile))
               {
                  LogMsg("***** Error removing temp file: %s", sTmpFile);
                  iErrno = errno;
                  break;
               }
            }
         }

         iRet = _findnext(lHandle, &sFileInfo);
      } while (!iRet);
      _findclose(lHandle);
   }

   return iErrno;
}

/*********************************** Check **********************************
 *
 ****************************************************************************/

int Check(bool bCorrect, LPCSTR pMsg)
{
   if ( ! bCorrect )
   {
      LogMsg0("***** Error: %s", pMsg);
      return 0;
   }
   return 1;
}

/******************************* RenameToExt ********************************
 *
 * Rename input file to provided extension
 *
 * Return 0 if success
 *
 ****************************************************************************/

int RenameToExt(LPSTR pFilename, LPSTR pExt)
{
   char  sTmp[_MAX_PATH], *pTmp;
   int   iRet=0;

   if (!_access(pFilename, 0))
   {
      strcpy(sTmp, pFilename);
      if (pTmp = strchr(sTmp, '.'))
         strcpy(pTmp+1, pExt);
      else
         sprintf(sTmp, "%s.%s", pFilename, pExt);

      if (!_access(sTmp, 0))
         iRet = DeleteFile(sTmp);

      // Rename this file
      iRet = rename(pFilename, sTmp);
   }

   return iRet;
}

/******************************* RebuildCsv ********************************
 *
 * Rebuild CSV file and validate that each row has a certain number of tokens.
 *
 * Input file must be a CSV file.
 *
 * Return number of output records if success, <0 if error.
 *
 ****************************************************************************/

int RebuildCsv(LPSTR pInfile, LPSTR pOutfile, unsigned char cDelim, int iTokens)
{
   int   iCnt=0, iRet;
   char  sBuf[4096];
   FILE  *fdInput, *fdOutput;

   LogMsg("Rebuild CSV %s ==> %s", pInfile, pOutfile);

   // Verify file open OK
   fdInput = fopen(pInfile, "r");
   if (!fdInput)
   {
      LogMsg("***** Cannot open input file %s\n", pInfile);
      return -1;
   }

   fdOutput = fopen(pOutfile, "w");
   if (!fdOutput)
   {
      fclose(fdInput);
      LogMsg("***** Cannot open output file %s\n", pOutfile);
      return -2;
   }
            
   while (!feof(fdInput))
   {
#ifdef _DEBUG
      //if (!memcmp(sBuf, "910102104000", 12))
      //   iRet = 0;
#endif
      iRet = myGetStrTC(sBuf, cDelim, iTokens, 4096, fdInput);
      if (iRet > 0)
      {
         strcat(sBuf, "\n");     // Put CRLF back on
         fputs(sBuf, fdOutput);
      } else
      {
         if (iRet == -2)
            LogMsg("***** Buffer is too small");
         else if (iRet == -3)
            LogMsg("*** Too many tokens in row: %d", iCnt);
         break;
      }

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   fclose(fdInput);
   fclose(fdOutput);

   return iCnt;
}

/***************************** InitUtils *************************************
 *
 * Initialize m_sDataSrc & m_sMachine
 * To avoid infinite loop, do not use GetIniString() in InitUtils()
 *
 *****************************************************************************/

void InitUtils(LPCSTR pIniFile)
{
   strcpy(m_sMachine, getenv("COMPUTERNAME"));
   // If DataSrc not defined, default to local machine
   GetPrivateProfileString("System", "CO_DATA", m_sMachine, m_sDataSrc, 64, pIniFile);
   replStr(m_sDataSrc, "[Machine]", m_sMachine);

}

/***************************** GetIniString **********************************
 *
 * Get a string item from INI file and translate to appropriate machine path
 *
 *****************************************************************************/

int GetIniString(LPCSTR pSection, LPCSTR pItem, LPCSTR pDefVal, LPSTR pVal, int iMaxBuf, LPCSTR pIniFile)
{
   char  sTmp[1024];
   int   iRet;

   if (m_sMachine[0] <= ' ')
      InitUtils(pIniFile);

   iRet = GetPrivateProfileString(pSection, pItem, pDefVal, sTmp, iMaxBuf, pIniFile);
   if (iRet > 0)
   {
      if (!memcmp(sTmp, "[CO_DATA]", 9))
         iRet = replStr(sTmp, "[CO_DATA]", m_sDataSrc);
      else if (strstr(sTmp, "[Machine]"))
         iRet = replStr(sTmp, "[Machine]", m_sMachine);
      strcpy(pVal, sTmp);
   } else
      *pVal = 0;

   return iRet;
}

/***************************** FixTxtFile ************************************
 *
 * Fix broken record file, use RebuildCsv
 *
 *****************************************************************************/

int FixTxtFile(LPCSTR pInfile, LPCSTR pOutfile, int iType)
{
   return 0;
}

/******************************* vmemcpy *************************************
 *
 * Copy upto specific number of bytes
 * Return number of bytes copied
 *
 *****************************************************************************/

int vmemcpy(LPSTR pDest, LPSTR pSrc, int iMaxLen, int iStrLen)
{
   int   iTmp;
   
   if (iStrLen <= 0)
      iTmp = strlen(pSrc);
   else
      iTmp = iStrLen;

   if (iTmp > iMaxLen)
      iTmp = iMaxLen;
   memcpy(pDest, pSrc, iTmp);
   return iTmp;
}

/******************************** SafeDelete() ********************************
 * Delete file by moving it recycle bin
 ******************************************************************************/

int SafeDelete(LPCSTR pFilepath)
{
   SHFILEOPSTRUCT shfos = {};
   shfos.hwnd   = nullptr;       // handle to window that will own generated windows, if applicable
   shfos.wFunc  = FO_DELETE;
   shfos.pFrom  = pFilepath;
   shfos.pTo    = nullptr;       // not used for deletion operations
   shfos.fFlags = FOF_ALLOWUNDO | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT; // use the recycle bin

   int iRet;
   
   try
   {
      iRet = SHFileOperation(&shfos);
      if (iRet != 0)
         LogMsg("***** Error removing file: %s (0x%.x)", pFilepath, iRet);
   } catch (...)
   {
      LogMsg("***** Error removing file: %s (0x%.x)", pFilepath, iRet);
   }
   return iRet;
}

/************************************ GetExePath *******************************
 *
 * Find path of running program.
 *
 *******************************************************************************/

char *GetExePath(LPSTR pExePath, LPCSTR pArg0)
{
    char *pTmp, basePath[255] = "";

    _fullpath(basePath, pArg0, sizeof(basePath));

    if (pTmp = strrchr(basePath, '\\'))
    {
       *pTmp = 0;
       strcpy(pExePath, basePath);
    } else
       *pExePath = 0;

    return pExePath;
}

/************************************ _strstrn *********************************
 *
 * Find string within fix length string.
 *
 *******************************************************************************/

char *_strstrn(char *pString, char *pSearch, int iMaxLen)
{
   char  *pRet, sTmp[2048];
   int   iTmp;

   memcpy(sTmp, pString, iMaxLen);
   sTmp[iMaxLen] = 0;
   if (pRet = strstr(sTmp, pSearch))
   {
      iTmp = pRet - sTmp;
      pRet = pString+iTmp;
   }

   return pRet;
}